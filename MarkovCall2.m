function MarkovCall2

%Male P8
files= {409;411;418;438;440;443;449;462;489;410;415;444;448;452;456;457;458;459;464;486;487;488};
%Female P8
%files= {388;416;420;434;450;485;412;413;419;421;422;433;445;446;451;460;461;480;481;482;483;484;490};
%Male P12
%files= {411;414;418;438;440;410;439;448;449;452;456;458;459};
% EMPTY = {HT-415};
%Female P12
%files= {324;326;336;340;416;417;434;450;325;337;338;339;412;413;419;451;461};
% EMPTY = {HT-337;338;412;413;419};


% Set up Excel Spreadsheet for the data
Headers={'File','Call','Cx','Ha','Ts','U','D','H','Sh','C','Fs','F','Start','End','Isolated'};
%XLS=zeros([length(files)*10 15]);
%XLS(j*10-9,1)=cell2mat(files(j,1));
XLS=zeros([length(files) 3]);
Name={'Cx';'Ha';'Ts';'U';'D';'H';'Sh';'C';'Fs';'F'};

% Establish Window Sizes that should be tested
Window=[0.2 0.5 1];

for j=1:length(files);
    
[num,txt,raw] = xlsread(['Z:\Mouse Calls\Data\Format\P8\Male\M_A' num2str(cell2mat(files(j,1))) '_P8']);
k=files(j,1);
Title=num2str(cell2mat(k));

Tally=zeros([1 10]);



for g=1:length(Window);
Burst=0;

for h=1:10;
    Cx=0;
    Ha=0;
    Ts=0;
    U=0;
    D=0;
    H=0;
    Sh=0;
    C=0;
    Fs=0;
    F=0;
    Start=0;
    End=0;
    Isolated=0;

    if h==8;
        H=0;
    end
    
    list=find(num(:,2)==h);
    for i=1:length(list);
        if num(list(i),1)-num(list(i)-1,1)>=Window(g);
            Start=Start+1;
            Burst=Burst+1;
        end
        if num(list(i)+1,1)-num(list(i),1)>=Window(g);
            End=End+1;
            if num(list(i),1)-num(list(i)-1,1)>=1&&num(list(i)+1,1)-num(list(i),1)>=1;
                Isolated=Isolated+1;
                Start=Start-1;
                End=End-1;
                Burst=Burst-1;
            end
        else
            if num(list(i)+1,2)==1;
                Cx=Cx+1;
            end
            if num(list(i)+1,2)==2;
                Ha=Ha+1;
            end
            if num(list(i)+1,2)==3;
                Ts=Ts+1;
            end
            if num(list(i)+1,2)==4;
                U=U+1;
            end
            if num(list(i)+1,2)==5;
                D=D+1;
            end
            if num(list(i)+1,2)==6;
                H=H+1;
            end
            if num(list(i)+1,2)==7;
                Sh=Sh+1;
            end
            if num(list(i)+1,2)==8;
                C=C+1;
            end
            if num(list(i)+1,2)==9;
                Fs=Fs+1;
            end
            if num(list(i)+1,2)==10;
                F=F+1;
            end
        end
    end
    
    Tally=([Cx Ha Ts U D H Sh C Fs F]);
    Total=sum(Tally);
    Proportion=Tally/Total;
    Proportion(isnan(Proportion))=0;
    %Add values to spreadsheet
    %XLS((((j*10)-9)+h-1),3:12)=Proportion;
    %XLS((((j*10)-9)+h-1),3:12)=Tally;
    %XLS((((j*10)-9)+h-1),13)=Start;
    %XLS((((j*10)-9)+h-1),14)=End;
    %XLS((((j*10)-9)+h-1),15)=Isolated;
    %XLS((((j*10)-9)+h-1),1)=Burst;
    XLS(j,g)=Burst;
    
end
%print
%xlswrite(['MP_F_P12_1.0_tot.xls'],XLS,'Sheet' num2str(g),'A2');
%xlswrite(['MP_F_P12_1.0_tot.xls'],Headers,'Sheet' num2str(g),'A1');
%xlswrite(['MP_F_P12_1.0_tot.xls'],Name,'Sheet' num2str(g),'B2');
xlswrite(['Burst_M_P8.xls'],files,'Sheet1','A1');
xlswrite(['Burst_M_P8.xls'],XLS,'Sheet1','B1');
end
end
