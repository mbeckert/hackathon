cd('f:\desktop\Data\hiroi')
load('f:\desktop\Data\hiroi\info\callkey_REV.mat')

files = dir;
files = {files.name};
files = files';

pattern = 'GKRevised.';

idx = regexpi(files,pattern);
idx = ~cellfun(@isempty,idx);
files = files(idx);

clear idx pattern

COUNT = cell(length(files),1);
MARK = cell(length(files),1);
SEQ = cell(length(files),1);
BURSTS = cell(length(files),1);
ANIMAL_ID = nan(length(files),1);

for f = 1:length(files)
    if sum(~cellfun(@isempty,regexpi(files{f},IDs.WT))) >= 1
        ANIMAL_ID(f) = 1;
        cutoff = crosspoint.wt;
    elseif sum(~cellfun(@isempty,regexpi(files{f},IDs.HT))) >= 1
        ANIMAL_ID(f) = 0;
        cutoff = crosspoint.ht;
    else
        ANIMAL_ID(f) = nan;
        cutoff = 0;
    end
    
[num, ~] = xlsread(files{f},4,'E1:F1200');
ICI = num(:,1);

[~, CALLS] = xlsread(files{f},4,'H1:H1200');
CALLS = CALLS(2:end);

if length(ICI) ~= length(CALLS)
    tmp = [ICI; nan(length(CALLS) - length(ICI),1)];
    clear ICI
    ICI = tmp;
    clear tmp
    tmp = [CALLS; cell(length(ICI) - length(CALLS),1)];
    clear CALLS
    CALLS = tmp;
    clear tmp
end
key = nan(length(CALLS),1);

chck = zeros(size(key));

for c = 1:length(callkey)
    idx = regexpi(CALLS,callkey{c});
    idx = ~cellfun(@isempty,idx);
    chck = chck + idx;
    if c > 10
        key(idx) = 11;
    else
        key(idx) = c;
    end
end

idx = chck > 1;
key(idx) = 11;

chckKEY = isnan(key); chckICI = isnan(ICI);
crossCHCK = chckKEY + chckICI;
crossCHCK = find(crossCHCK == 1);
crossCHCK(1) = [];

idx = find(diff(crossCHCK) == 1);
for i = 1:length(idx)
    key(crossCHCK(idx(i)):crossCHCK(idx(i))+1) = flipud(key(crossCHCK(idx(i)):crossCHCK(idx(i))+1));
end

clear chckKEY chckICI crossCHCK idx i
idx = isnan(key);
key(idx) = [];
ICI(idx) = [];
ICI(length(key)+1:end) = [];

clear idx chck c txt

COUNT{f} = unique_counts(key,1:11);
BURSTS{f} = nan(200,1);
SEQ{f} = cell(500,1);
MARK{f} = zeros(11);
b_count = 1;
seq_hold = nan(300,1);

for c = 2:length(key)-1
    if ICI(c) < cutoff
        b_count = b_count + 1;
        MARK{f}(key(c-1),key(c)) = MARK{f}(key(c-1),key(c)) + 1;
        seq_hold(find(isnan(seq_hold),1,'first')) = key(c-1);
    else
        BURSTS{f}(find(isnan(BURSTS{f}),1,'first')) = b_count;
        b_count = 1;
        seq_hold(find(isnan(seq_hold),1,'first')) = key(c-1);
        seq_hold(find(isnan(seq_hold),1,'first'):end) = [];
        SEQ{f}{find(cellfun(@isempty,SEQ{f}),1,'first')} = seq_hold;
        seq_hold = nan(300,1);
    end
end

if ICI(end) < cutoff
    b_count = b_count + 2;
    MARK{f}(key(end-1),key(end)) = MARK{f}(key(end-1),key(end)) + 1;
    seq_hold(find(isnan(seq_hold),1,'first')) = key(end-1);
    seq_hold(find(isnan(seq_hold),1,'first')) = key(end);
    seq_hold(find(isnan(seq_hold),1,'first'):end) = [];
    SEQ{f}{find(cellfun(@isempty,SEQ{f}),1,'first')} = seq_hold;
    BURSTS{f}(find(isnan(BURSTS{f}),1,'first')) = b_count;
else
    seq_hold(find(isnan(seq_hold),1,'first')) = key(end-1);
    seq_hold(find(isnan(seq_hold),1,'first'):end) = [];
    SEQ{f}{find(cellfun(@isempty,SEQ{f}),1,'first')} = seq_hold;
    BURSTS{f}(find(isnan(BURSTS{f}),1,'first')) = b_count;

    SEQ{f}{find(cellfun(@isempty,SEQ{f}),1,'first')} = key(end);
    BURSTS{f}(find(isnan(BURSTS{f}),1,'first')) = 1;
end

SEQ{f}(find(cellfun(@isempty,SEQ{f}),1,'first'):end) = [];
BURSTS{f}(find(isnan(BURSTS{f}),1,'first'):end) = [];

clear ICI CALLS num c b_count key cutoff seq_hold
end

clear f callkey crosspoint IDs

ANIMAL_ID = logical(ANIMAL_ID);

cd('F:\Desktop\WorkingFolder')