cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected_genotypes2.mat','files','COUNT','ANIMAL_ID')
load('f:\desktop\Data\hiroi\info\callkey_REV.mat','callkey')
cd('f:\desktop\Data\hiroi')

count_compare = cell(size(COUNT));

for f = 1:length(files)
    [num,CALLS] = xlsread(files{f},2,'J1:K12');
    if ~isempty(CALLS)
        tmp_calls = cell(size(callkey));
        tmp_counts = nan(size(num));
        for c = 1:length(callkey)
            idx = regexpi(CALLS,callkey{c});
            idx = ~cellfun(@isempty,idx);
            tmp_calls(c) = CALLS(idx);
            tmp_counts(c) = num(idx);
        end
        tmp_counts(11) = tmp_counts(11) + tmp_counts(12);
        tmp_counts(12) = [];
        count_compare{f} = [COUNT{f},tmp_counts,abs(tmp_counts - COUNT{f}),nan(11,1)];
        
    else
        count_compare{f} = nan;
    end
    clear num CALLS c tmp_counts idx
end
clear f

tmp_calls(11) = [];
y = cellfun(@(x) sum(x(:,3)), count_compare, 'UniformOutput', 0);
x = cell(size(files));
x(ANIMAL_ID) = {'wt'};
x(~ANIMAL_ID) = {'ht'};
headerwt = [files(ANIMAL_ID), x(ANIMAL_ID), y(ANIMAL_ID), num2cell(nan(sum(ANIMAL_ID),1))];
headerwt = reshape(headerwt',[1, size(headerwt,1) * size(headerwt,2)]);
datawt = cell2mat(count_compare(ANIMAL_ID)');
dataht = cell2mat(count_compare(~ANIMAL_ID)');
headerht = [files(~ANIMAL_ID), x(~ANIMAL_ID), y(~ANIMAL_ID), num2cell(nan(sum(~ANIMAL_ID),1))];
headerht = reshape(headerht',[1, size(headerht,1) * size(headerht,2)]);

cd('F:\Desktop\WorkingFolder')
xlswrite('results_confirmation.xlsx', headerwt, 1, 'B1')
xlswrite('results_confirmation.xlsx', tmp_calls, 1, 'A2')
xlswrite('results_confirmation.xlsx', datawt, 1, 'B2');
xlswrite('results_confirmation.xlsx', headerht, 1, 'B14')
xlswrite('results_confirmation.xlsx', tmp_calls, 1, 'A15')
xlswrite('results_confirmation.xlsx', dataht, 1, 'B15');

clear

cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected_genotypes2.mat','ANIMAL_ID','files','BURSTS','SEQ')
names = cellfun(@(x) x(20:22), files, 'UniformOutput', 0);
calls = {'complex','harmonics','2-syllable','upwards','downwards','chevron','short','composite','frequency steps','flat','ambiguous'};

for f = 1:length(files)
    data = SEQ{f};
    for b = 1:length(data)
        SEQ{f}{b} = calls(data{b});
    end
end

for f = 1:length(files)
    data = SEQ{f};
    m = max(cellfun(@length, SEQ));
    out = cell(size(data,1),m);
    for i = 1:length(data)
        out(i,:) = [data{i}, cell(1, m - length(data{i}))];
    end
    xlswrite('results_sequences.xlsx', files(f), names{f}, 'A1')
    xlswrite('results_sequences.xlsx', out, names{f}, 'A2')
end

clear

cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected_genotypes2.mat','ANIMAL_ID','files','BURSTS','MARK')
names = cellfun(@(x) x(20:22), files, 'UniformOutput', 0);
ID = cell(length(ANIMAL_ID),1);
ID(ANIMAL_ID) = {'wt'};
ID(~ANIMAL_ID) = {'ht'};
MARK2 = cellfun(@(x) x ./ repmat(sum(x,2),[1 11]), MARK, 'UniformOutput', 0);

for f = 1:length(files)
    
    MARK2{f}(isnan(MARK2{f})) = 0;
    headerwt = {files{f};ID{f}};
    data = [MARK{f}, nan(11,1), MARK2{f}];
    bur = BURSTS{f}';
    
    xlswrite('results_individualdata.xlsx', headerwt, names{f}, 'A1')
    xlswrite('results_individualdata.xlsx', data, names{f}, 'A4')
    xlswrite('results_individualdata.xlsx', bur, names{f}, 'A16');
    
end

clear f header data bur headerwt MARK BURSTS

cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected_genotypes2.mat')
load('markov_16HDel_corrected_genotypes2_analyzed.mat')

calls = {'complex','harmonics','2-syllable','upwards','downwards','chevron','short','composite','frequency steps','flat','ambiguous'};

txt = {'WT MEAN', 'HT MEAN', 'WT MEDIAN', 'HT MEDIAN'};
coord.name = {'A1', 'O1', 'A16', 'O16'};
coord.values = {'B3', 'P3', 'B17', 'P17'};
values = {prob_across.meanwt, prob_across.meanht, prob_across.medwt, prob_across.medht};

for i = 1:length(txt)
    xlswrite('results_group.xlsx', txt(i), 'Probabilities', coord.name{i})
    xlswrite('results_group.xlsx', values{i}, 'Probabilities', coord.values{i})
end
xlswrite('results_group.xlsx', calls, 'Probabilities', 'B2')
xlswrite('results_group.xlsx', calls', 'Probabilities', 'A3')

xlswrite('results_group.xlsx', calls, 'Mann-whitney', 'B2')
xlswrite('results_group.xlsx', calls', 'Mann-Whitney', 'A3')
xlswrite('results_group.xlsx', stats.mannwhitney, 'Mann-Whitney', 'B3')

text = {'file', 'correlation', 'distance'};
xlswrite('results_group.xlsx', text, 'Similarity measurements', 'A1')
xlswrite('results_group.xlsx', files.wt, 'Similarity measurements', 'A2')
xlswrite('results_group.xlsx', similar.wtcorr, 'Similarity measurements', 'B2')
xlswrite('results_group.xlsx', similar.wtdiff, 'Similarity measurements', 'C2')
xlswrite('results_group.xlsx', text, 'Similarity measurements', 'E1')
xlswrite('results_group.xlsx', files.ht, 'Similarity measurements', 'E2')
xlswrite('results_group.xlsx', similar.htcorr, 'Similarity measurements', 'F2')
xlswrite('results_group.xlsx', similar.htdiff, 'Similarity measurements', 'G2')