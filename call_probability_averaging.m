function [markovprobabilities] = call_probability_averaging

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% Computes the average probability of Markov states 
% Run "call_probability.m" first
%
% Save output, output should contain the probabilities for each animal in a
% structure called "callprob" but if you have changed the variable name
% please modify the call to in line 30 and 33.
%
% The output will be a structure with each genotype in cells. The genotype
% name (Excel spreadsheet name) will be in the field "filename" and the
% averages will be in the field "averages"
%
% "averages" is a matrix where rows is the first call and columns the
% second. 
% For example, the probability for a Cx-U call pair is in row 1, column 4 
%
%   - written Jan 6, 2018 - Michael Beckert
%
%   QUESTIONS and COMMENTS should be addressed to Dr. Noboru Hiroi,
%   noboru.hiroi@einstein.yu.edu
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% User input file to open which was saved after running "call_probability"
disp('Select raw probability file: ')
f = uigetfile;
load(f,'callprob')

% Only use this line if you have saved the file under a different name
% callprob = YOURFILENAME;    

% Assign final output
markovprobabilities = cell(size(callprob));

% Run through each genotype
for g = 1:length(callprob)
    
    % Determine how many animals
    fields = fieldnames(callprob{g});
    
    % Create a holder to tally up the probabilities
    holder = zeros(10,10);
    count = 0;
    
    % Run through each animal
    for a = 2:length(fields)
        % If this animal was valid then add to tally holder and counter 
        if isnumeric(callprob{g}.(fields{a}))
            holder = holder + callprob{g}.(fields{a});
            count = count + 1;
        end
    end
    
    % Average
    holder = holder / count;
    
    % Create output fields
    markovprobabilities{g}.filename = callprob{g}.filename;
    markovprobabilities{g}.averages = holder;
    
    clear count
end


    