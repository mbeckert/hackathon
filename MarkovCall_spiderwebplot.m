%% Circle plot

cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected.mat','COUNT','ANIMAL_ID')
count{1} = mean(cell2mat(cellfun(@(x) x/sum(x), COUNT(ANIMAL_ID), 'UniformOutput', 0)'),2)*1.5;
count{2} = mean(cell2mat(cellfun(@(x) x/sum(x), COUNT(~ANIMAL_ID), 'UniformOutput', 0)'),2)*1.5;
clear COUNT ANIMAL_ID

load('markov_16HDel_corrected_analyzed.mat','prob_all')

% FLAGS
cut_q = 0.5;     % Set a threshold for display (e.g. 0.1)
t = 1;              % What data to use, WT or HT
order = 'sort3';    % What order would you like the calltypes in?
                    % 'sort1' - predetermined order (values can be 1:3
                    % 'rand' - randomize
                    % 'orig' - original order, (default)

if t == 1
    data = prob_all.meanwt;
elseif t == 2
    data = prob_all.meanht;
end
cutoff = quantile(data(:),cut_q);
data(data < cutoff) = nan;
ma = max(data(:));
abbr = {'Cx', 'Ha', 'Ts', 'U', 'D', 'Ch', 'Sh', 'C', 'Fs', 'F', 'Am'};
% abbr = {'1','2','3','4','5','6','7','8','9','10','11'};

col = [0.9 0.6 0.6; ...
    0.9 0.6 0.6; ...
    0.9 0.6 0.6; ...
    0.9 0.6 0.6; ...
    0.9 0.6 0.6; ...
    0.6 0.6 0.9; ...
    0.6 0.6 0.9; ...
    0.6 0.6 0.9; ...
    0.6 0.6 0.9; ...
    0.6 0.6 0.9; ...
    0.8 0.8 0.8];

circ = 2*pi*(0:length(abbr))/length(abbr);
x = fliplr(2 * sin(circ(1:end-1)))';
y = fliplr(2 * cos(circ(1:end-1)))';
circcoor = cell(size(abbr));
circcoor2 = cell(size(abbr));
angles = linspace(0,360,length(abbr)+1);
angles = fliplr(angles(1:end-1));

% Set the order of the symbols
if strcmp(order,'rand')
    idx = randperm(length(x));
elseif strcmp(order,'sort1')
    [~,idx] = sort(count{t});
elseif strcmp(order,'sort2')
    idx = [1 6 2 8 7 11 5 3 4 9 10];
elseif strcmp(order,'sort3')
    idx = [10 7 9 1 3 5 4 6 8 2 11];
elseif strcmp(order,'orig')
    idx = 1:11;
end

x = x(idx); y = y(idx); col = col(idx,:); angles = angles(idx);

figure
hold on

for c = 1:length(abbr)
    [circcoor{c}(1,:),circcoor{c}(2,:)] = getCircleCoor(x(c),y(c),0.01,count{t}(c)*1.2);
    [circcoor2{c}(1,:),circcoor2{c}(2,:)] = getCircleCoor(x(c),y(c),0.01,count{t}(c)*1.2+0.03);
    cir = patch(circcoor{c}(1,:),circcoor{c}(2,:), col(c,:));
    alpha(cir,count{t}(c)/max(count{t}))
end

for c = 1:length(abbr)
    text(x(c), y(c), abbr{c}, ...
        'HorizontalAlignment', 'center', ...
        'FontName', 'TimesNewRoman', ...
        'FontSize', count{t}(c)*175)
end
axis([-3.5 3.5 -3.5 3.5])
yticklabels('')
xticklabels('')
set(gca,'Visible','off')
axis square

for c = 1:length(abbr)
    
    start = [x, y];
    stop = [repmat(x(c),length(abbr),1), repmat(y(c),length(abbr),1)];

    direction = ones(length(abbr),1);
    direction(c:end) = 0;
    
    for cc = 1:size(start,1)
        
        new = InterX([start(cc,:)',stop(cc,:)'],circcoor2{c});
        if ~isempty(new)
            stop(cc,1) = new(1); stop(cc,2) = new(2);
        end
        new = InterX([start(cc,:)',stop(cc,:)'],circcoor2{cc});
        if ~isempty(new)
            start(cc,1) = new(1); start(cc,2) = new(2);
        end
    end
    
    tmp = [x(c);y(c)];
    new = InterX([tmp, tmp*2],circcoor2{c});

    if ~isempty(new)
        stop(c,1) = new(1)*(1+count{t}(c)); stop(c,2) = new(2)*(1+count{t}(c));
        start(c,1) = new(1); start(c,2) = new(2);
    end
    
    lmat = data(:,c) * 300;
    
    idx = isnan(lmat);
%     idx(c) = 1;
    start(idx,:) = [];
    stop(idx,:) = [];
    lmat(idx) = [];
    colors = col;
    colors(idx,:) = [];
    abbr_tmp = abbr;
    abbr_tmp(idx) = [];
    direction(idx) = [];
    chan = 1:length(abbr);
    chan(idx) = [];
    
    if ~isempty(start)
        for i = 1:size(start,1)
            
            ang = rad2deg(atan(abs((start(i,2)-stop(i,2)))/abs((start(i,1)-stop(i,1)))));
            xCenter = (start(i,1) + stop(i,1)) / 2;
            yCenter = (start(i,2) + stop(i,2)) / 2;
            if stop(i,1) < start(i,1) && stop(i,2) < start(i,2)
                rotationAngle = 90 + ang;
            elseif stop(i,1) > start(i,1) && stop(i,2) < start(i,2)
                rotationAngle = 90 - ang;
            elseif stop(i,1) < start(i,1) && stop(i,2) > start(i,2)
                rotationAngle =  90 - ang;
            else
                rotationAngle = 90 - (-ang);
            end
            
            if direction(i) == 0
                rotationAngle = rotationAngle + 180;
            end

            if chan(i) == c 
                theta = linspace(80, 340, 200);
                if xCenter > 0
                    theta = linspace(-130, 150, 200);
                end
                yradius = count{t}(c)*1.3;
                xradius = count{t}(c)*0.8;
            else
                theta = linspace(190, 350, 200);
                yradius = (sqrt( (stop(i,2) - start(i,2))^2 + (stop(i,1) - start(i,1))^2)) / 2;
                xradius = 0.3;
            end
            
            % script found from "Image Analyst" on MatLab Answers
            transformMatrix = [cosd(rotationAngle), sind(rotationAngle);...
                -sind(rotationAngle), cosd(rotationAngle)];
            xAligned = xradius * sind(theta);
            yAligned = yradius * cosd(theta);
            xyAligned = [xAligned; yAligned]';
            xyRotated = xyAligned * transformMatrix;
            xRotated = xyRotated(:, 1) + xCenter;
            yRotated = xyRotated(:, 2) + yCenter;
            
            mm = abs([xRotated(1),xRotated(end)] - x(c));
            
            if chan(i) ~= c
            if find(mm == min(mm),1,'first') == 1
                xRotated = flipud(xRotated);
                yRotated = flipud(yRotated);
            end
            end
            
%             [arr.x,arr.y] = arrow([xRotated(end-5),yRotated(end-5)], [xRotated(end),yRotated(end)]);
%             new = InterX([xRotated(1:end-5)'; yRotated(1:end-5)'],[arr.x';arr.y']);
%             
%             ii = nan(2,1);
%             xx = abs(xRotated - new(1));
%             ii(1) = find(xx == min(xx), 1, 'last');
%             yy = abs(yRotated - new(2));
%             ii(2) = find(yy == min(yy), 1, 'last');
%             
%             if ii(1) ~= ii(2)
%                 disp(['line and arrow intersect does not match for ' num2str(c) '_' num2str(i)])
%             end
%             
%             n = round(mean(ii));
            
            alphafactor = (lmat(i) / (ma * 300));

            if chan(i) == c
                n = 30;
            else
                n = 5;
            end
            
            lin = patchline(xRotated(1:end-n), yRotated(1:end-n), ...
                'LineWidth', lmat(i,:), ...
                'EdgeColor', colors(i,:), ...
                'FaceColor', colors(i,:), ...
                'FaceAlpha', alphafactor, ...
                'EdgeAlpha', alphafactor);
            arrow([xRotated(end-n),yRotated(end-n)], [xRotated(end),yRotated(end)], ...
                'width', lmat(i,:), ...
                'BaseAngle', 90, ...
                'TipAngle', 40, ...
                'Length', lmat(i,:), ...
                'EdgeColor', colors(i,:), ...
                'FaceColor', colors(i,:), ...
                'FaceAlpha', 1, ...
                'EdgeAlpha', 1);
%                 'EdgeColor', colors(i,:) + ((1 - colors(i,:)) * alphafactor/2), ...
%                 'FaceColor', colors(i,:) + ((1 - colors(i,:)) * alphafactor/2), ...
        end
        
    end
    
end

if t == 1
    text(-3.5,3.5,['WT - ' num2str(1 - cut_q)])
elseif t == 2
    text(-3.5,3.5,['HT - ' num2str(1 - cut_q)])
end

% clear c i lmat s start stop x y circ idx col cutoff cc new t colors ii ttl

