cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected_analyzed.mat','prob_across');

data = nan(4,4);

for i = 2

cd('H:\WarehouseData\Mouse Calls\Markov');
[x,~] = xlsread('Raw.xlsx', i, 'Y2:AH11');

y = prob_across.meanwt(1:10,1:10);

figure(i)
subplot(2,2,1)
imagesc(x)
subplot(2,2,2)
imagesc(y)
subplot(2,2,3)
scatter(x(:),y(:),'r','.')
hold on
xx = x(2:10,2:10);
yy = y(2:10,2:10);
scatter(xx(:),yy(:),'k','.')
data(i,1) = corr(x(:),y(:));
data(i,2) = corr(xx(:),yy(:));

y = prob_across.meanht(1:10,1:10);

subplot(2,2,4)
scatter(x(:),y(:),'r','.')
hold on
xx = x(2:10,2:10);
yy = y(2:10,2:10);
scatter(xx(:),yy(:),'k','.')
data(i,3) = corr(x(:),y(:));
data(i,4) = corr(xx(:),yy(:));

end

clear i x xx y yy

cd('F:\Desktop\WorkingFolder')

