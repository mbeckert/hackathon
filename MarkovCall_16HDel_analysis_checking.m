%% Check if the calls used in the analysis match the excel sheets
cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected.mat','files','COUNT','ANIMAL_ID')
load('f:\desktop\Data\hiroi call data\info\callkey.mat','callkey')
cd('f:\desktop\Data\hiroi call data')

count_compare = cell(size(COUNT));

for f = 1:length(files)
    [num,CALLS] = xlsread(files{f},2,'J1:K12');
    if ~isempty(CALLS)
        tmp_calls = cell(size(callkey));
        tmp_counts = nan(size(num));
        for c = 1:length(callkey)
            idx = regexpi(CALLS,callkey{c});
            idx = ~cellfun(@isempty,idx);
            tmp_calls(c) = CALLS(idx);
            tmp_counts(c) = num(idx);
        end
        tmp_counts(11) = tmp_counts(11) + tmp_counts(12);
        tmp_counts(12) = [];
        count_compare{f} = abs(tmp_counts - COUNT{f});
        
    else
        count_compare{f} = nan;
    end
    clear num CALLS c tmp_counts tmp_calls idx 
end
clear f 

count_compare = cell2mat(cellfun(@sum,count_compare,'UniformOutput',0));
errors = sum(count_compare);

% % Some basic analysis
cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected.mat','ANIMAL_ID','BURSTS')

% Get distribution of burst length
burst_analysis.length_wt = cell2mat(BURSTS(ANIMAL_ID));
burst_analysis.length_ht = cell2mat(BURSTS(~ANIMAL_ID));

% Calculate the proportion of bursts the animal made compared to single
% calls
burst_analysis.burstprop_wt = cellfun(@(x) sum(x ~= 1)/length(x), BURSTS(ANIMAL_ID));
burst_analysis.burstprop_ht = cellfun(@(x) sum(x ~= 1)/length(x), BURSTS(~ANIMAL_ID));

clear BURSTS
% %
cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected.mat','ANIMAL_ID','MARK','files')
% Convert to the proportion of call transitions for each animal, then
% calculate mean occurrences
tmp = cellfun(@(x) x/sum(x(:)), MARK, 'UniformOutput', 0);
tmp2 = cellfun(@(x) x ./ repmat(sum(x,2),[1 11]), MARK, 'UniformOutput', 0);
MARK_prop = nan(11,11,length(tmp));
MARK_propacross = nan(11,11,length(tmp));
for i = 1:length(MARK_prop)
    MARK_prop(:,:,i) = tmp{i};
    MARK_propacross(:,:,i) = tmp2{i};
end
clear tmp i tmp2
prob_all.meanwt = nanmean(MARK_prop(:,:,ANIMAL_ID),3);
prob_all.meanht = nanmean(MARK_prop(:,:,~ANIMAL_ID),3);
prob_all.medwt = nanmedian(MARK_prop(:,:,ANIMAL_ID),3);
prob_all.medht = nanmedian(MARK_prop(:,:,~ANIMAL_ID),3);
prob_across.meanwt = nanmean(MARK_propacross(:,:,ANIMAL_ID),3);
prob_across.meanht = nanmean(MARK_propacross(:,:,~ANIMAL_ID),3);
prob_across.medwt = nanmedian(MARK_propacross(:,:,ANIMAL_ID),3);
prob_across.medht = nanmedian(MARK_propacross(:,:,~ANIMAL_ID),3);
% figure(1)
% subplot(2,2,1)
% imagesc(probabilities.wt)
% subplot(2,2,2)
% imagesc(probabilities.ht)
% subplot(2,2,3)
% scatter(probabilities.meanwt(:), probabilities.meanht(:));
disp(corr(prob_all.meanwt(:),prob_all.meanht(:)));
disp(corr(prob_across.meanwt(:),prob_across.meanht(:)));
tmp = files;
clear files

files.wt = tmp(ANIMAL_ID);
files.ht = tmp(~ANIMAL_ID);

similarity = nan(length(ANIMAL_ID),1);
euclidean = nan(length(ANIMAL_ID),1);

for i = 1:length(similarity)
    tmp = MARK_prop(:,:,i);
    if ANIMAL_ID(i) == 1
        base = prob_all.meanwt;
    elseif ANIMAL_ID(i) == 0
        base = prob_all.meanht;
    end
    similarity(i) = corr(tmp(:),base(:));
    euclidean(i) = sum(abs(tmp(:) - base(:)));
end

prob_all.meandiff = prob_all.meanwt - prob_all.meanht;
prob_all.meddiff = prob_all.medwt - prob_all.medht;
prob_across.meandiff = prob_across.meanwt - prob_across.meanht;
prob_across.meddiff = prob_across.medwt - prob_across.medht;
similar.wtcorr = similarity(ANIMAL_ID);
similar.htcorr = similarity(~ANIMAL_ID);
similar.wtdiff = euclidean(ANIMAL_ID);
similar.htdiff = euclidean(~ANIMAL_ID);

clear i tmp base similarity euclidean

[~,b] = sort(similar.wtcorr);
best.wtc = b(end-2:end-1);
[~,b] = sort(similar.wtdiff);
best.wtd = b(1:2);
[~,b] = sort(similar.htcorr);
best.htc = b(end-1:end);
[~,b] = sort(similar.htdiff);
best.htd = b(1:2);

clear b 

stats.mannwhitney = nan(11,11);
% count = 1;
for r = 1:size(stats.mannwhitney,1)
    for c = 1:size(stats.mannwhitney,2)
        a = MARK_prop(r,c,ANIMAL_ID);
        a = reshape(a,[length(a),1]);
        b = MARK_prop(r,c,~ANIMAL_ID);
        b = reshape(b,[length(b),1]);
        
        stats.mannwhitney(r,c) = ranksum(a,b);
%         d = [b; nan(length(a) - length(b),1)];
%         subplot(11,11,count)
%         boxplot([a,d]) 
%         count = count + 1;
    end
end

clear ANIMAL_ID SEQ SEQ_prop r c a b d errors count_compare
