cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected_analyzed.mat')

labels = {'complex', ...
'harmonics', ...
'2-syllable', ...
'upwards', ...
'downwards', ...
'chevron', ...
'short', ...
'composite', ...
'frequency steps', ...
'flat', ...
'ambiguous' ...
};

figure(1)

subplot(2,2,1)
imagesc(prob_across.meanwt,[0 0.5])
title('WT')
xticks(1:11)
xticklabels(labels)
xtickangle(45)
xlabel('second call')
yticks(1:11)
yticklabels(labels)
ylabel('first call')
axis square
colorbar

subplot(2,2,2)
imagesc(prob_across.meanht,[0 0.5])
title('HT')
xticks(1:11)
xticklabels(labels)
xtickangle(45)
xlabel('second call')
yticks(1:11)
yticklabels(labels)
ylabel('first call')
axis square
colorbar

subplot(2,2,3)
scatter(prob_across.meanwt(:), prob_across.meanht(:), 5, 'k', 'fill')
title('Correlation')
xlabel('wt')
ylabel('ht')
axis square
axis([0 0.4 0 0.4])
text(0.3, 0.05, ['r = ' num2str(corr(prob_across.meanwt(:), prob_across.meanht(:)))])

subplot(2,2,4)
cd('H:\WarehouseData\Mouse Calls\Markov');
[x,~] = xlsread('Raw.xlsx', 2, 'Y2:AH11');
imagesc(x,[0 0.5]);
title('WT from Tbx1')
xticks(1:10)
xticklabels(labels(1:10))
xtickangle(45)
xlabel('second call')
yticks(1:10)
yticklabels(labels(1:10))
ylabel('first call')
axis square
colorbar

