% navigate to folder where excel data files are stored
cd('f:\desktop\Data\hiroi call data')

% this file was made before and contains the animal ID (WT or HT) and cross
% points for the burst analysis
load('f:\desktop\Data\hiroi call data\info\callkey.mat')

%% Get the files in the folder then do some munging so you only have the files 
% you wish to work with.
files = dir;            % all files in directory
files = {files.name};   % just file names
files = files';         % transpose

pattern = 'GKRevised.';     % All necessary files have this lead text

idx = regexpi(files,pattern);   % Check just for these files and remove the rest
idx = ~cellfun(@isempty,idx);
files = files(idx);

clear idx pattern   % Clean up work space

%%

% Preallocate some holders for the data outputs
COUNT = cell(length(files),1);
MARK = cell(length(files),1);
SEQ = cell(length(files),1);
BURSTS = cell(length(files),1);
ANIMAL_ID = nan(length(files),1);

% Run through each file
for f = 1:length(files)
    % Depending on if it is a WT or HT change the cutoff point and keep
    % track of that for later
    if sum(~cellfun(@isempty,regexpi(files{f},IDs.WT))) >= 1
        ANIMAL_ID(f) = 1;
        cutoff = crosspoint.wt;
    elseif sum(~cellfun(@isempty,regexpi(files{f},IDs.HT))) >= 1
        ANIMAL_ID(f) = 0;
        cutoff = crosspoint.ht;
    else
        ANIMAL_ID(f) = nan;
        cutoff = 0;
    end
    
    % Read in the data, specifying particular region because some times
    % people place text or numbers in empty cells, which throws the output
    % off when we read it. Try not to have to work with Excel in this
    % manner whenever possible.
    
    % First just the times
[num, ~] = xlsread(files{f},4,'E1:F1200');
ICI = num(:,1);
    % Now the calls
[~, CALLS] = xlsread(files{f},4,'H1:H1200');
CALLS = CALLS(2:end);

    % Clean up to catch some special situations (again, because of how
    % excel data are handled reading them into MatLab can get messy)
if length(ICI) ~= length(CALLS)
    tmp = [ICI; nan(length(CALLS) - length(ICI),1)];
    ICI = tmp;
    tmp = [CALLS; cell(length(ICI) - length(CALLS),1)];
    CALLS = tmp;
    clear tmp
end
key = nan(length(CALLS),1);

chck = zeros(size(key));

for c = 1:length(callkey)
    idx = regexpi(CALLS,callkey{c});
    idx = ~cellfun(@isempty,idx);
    chck = chck + idx;
    if c > 10
        key(idx) = 11;
    else
        key(idx) = c;
    end
end

idx = chck > 1;
key(idx) = 11;

chckKEY = isnan(key); chckICI = isnan(ICI);
crossCHCK = chckKEY + chckICI;
crossCHCK = find(crossCHCK == 1);
crossCHCK(1) = [];

idx = find(diff(crossCHCK) == 1);
for i = 1:length(idx)
    key(crossCHCK(idx(i)):crossCHCK(idx(i))+1) = flipud(key(crossCHCK(idx(i)):crossCHCK(idx(i))+1));
end

clear chckKEY chckICI crossCHCK idx i
idx = isnan(key);
key(idx) = [];
ICI(idx) = [];
ICI(length(key)+1:end) = [];

clear idx chck c txt

COUNT{f} = unique_counts(key,1:11);
SEQ{f} = cell(500,1);
MARK{f} = zeros(11);
seq_hold = nan(300,1);

for c = 2:length(key)-1
    if ICI(c) < cutoff
        MARK{f}(key(c-1),key(c)) = MARK{f}(key(c-1),key(c)) + 1;
        seq_hold(find(isnan(seq_hold),1,'first')) = key(c-1);
    else
        seq_hold(find(isnan(seq_hold),1,'first')) = key(c-1);
        seq_hold(find(isnan(seq_hold),1,'first'):end) = [];
        SEQ{f}{find(cellfun(@isempty,SEQ{f}),1,'first')} = seq_hold;
        seq_hold = nan(300,1);
    end
end

if ICI(end) < cutoff
    MARK{f}(key(end-1),key(end)) = MARK{f}(key(end-1),key(end)) + 1;
    seq_hold(find(isnan(seq_hold),1,'first')) = key(end-1);
    seq_hold(find(isnan(seq_hold),1,'first')) = key(end);
    seq_hold(find(isnan(seq_hold),1,'first'):end) = [];
    SEQ{f}{find(cellfun(@isempty,SEQ{f}),1,'first')} = seq_hold;
else
    seq_hold(find(isnan(seq_hold),1,'first')) = key(end-1);
    seq_hold(find(isnan(seq_hold),1,'first'):end) = [];
    SEQ{f}{find(cellfun(@isempty,SEQ{f}),1,'first')} = seq_hold;

    SEQ{f}{find(cellfun(@isempty,SEQ{f}),1,'first')} = key(end);
end

SEQ{f}(find(cellfun(@isempty,SEQ{f}),1,'first'):end) = [];

clear ICI CALLS num c key cutoff seq_hold
end

for f = 1:length(SEQ)
    BURSTS{f} = cellfun(@length, SEQ{f});
end

clear f callkey crosspoint IDs

ANIMAL_ID = logical(ANIMAL_ID);

cd('F:\Desktop\WorkingFolder')