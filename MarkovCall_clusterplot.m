cd('F:\Desktop\WorkingFolder')
load('markov_16HDel_corrected_analyzed.mat','prob_across')

abbr = {'Cx', 'Ha', 'Ts', 'U', 'D', 'Ch', 'Sh', 'C', 'Fs', 'F', 'Am'};

data = prob_across.meanwt;
coord = (data - mean(data(:))) * 10;

key = combnk(1:11,2);

figure
hold on
for i = 1:size(key,1)
    scatter(coord(key(i,1),key(i,2)),coord(key(i,2),key(i,1)),100,'o','k');
end
