function [callprob] = call_probability(interval)

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%   This function tallies the Markov probabilities of call sequences.
%
%   Navigate to a folder containing only the Excel spreadsheets containing
%   the call sequence data and run this script.
%
%   "interval" is the specific intercall interval which defines a string of calls
%   "callprob" is the final output, each cell being a spreadsheet and each
%   field of that cell being the Markov probabilities. 
%
%   - written Oct 5, 2017 - Michael Beckert
%       - Jan 5, 2018 - added section to remove temporary files Excel makes
%               when the spreadsheet is open while running this script - MB
%
%   QUESTIONS and COMMENTS should be addressed to Dr. Noboru Hiroi,
%   noboru.hiroi@einstein.yu.edu
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%% Constants

% define the intercall interval to define a string in milliseconds 
%(default 200ms)
if ~exist('interval','var')
    interval = 200; % interval to define a string in milliseconds
end

% list of call names to compare to the Excel input
calls ={'Cx';'Ha';'Ts';'U';'D';'H';'Sh';'C';'Fs';'F'};

%% Gather file names from directory

files = dir;
files = {files(~[files.isdir]).name};

chck = cellfun(@(x) strcmp(x(1:2),'~$'), files);    % remove temporary files excel 
files = files(~chck);                               % makes if the spreadsheet is open

%% Create final data output holder
callprob = cell(length(files),1);

%% Run through files
for f = 1:length(files)
    
    callprob{f}.filename = files{f}; % create field to label this cell
    
    [~,sheets,~] = xlsfinfo(files{f}); % check for how many animals are in the spreadsheet

    % Run through animal sheets
    for s = 1:length(sheets)
        
        tally = nan(length(calls),length(calls));   % data holder
        [num,txt] = xlsread(files{f},sheets{s});    % extract data from excel sheet
        
        % Check for situations where analysis is not possible:
        % There must be at least 2 calls to run
        if size(num,1) > 1  
            
            % Delete parts of the data that are not needed
            num = num(:,4);
            txt = txt(2:end,2);
            
            % Create the index for the call types, converting the strings
            % to numerical labels
            call_index = cell(1,length(calls));
            for c = 1:length(calls)
                call_index{c} = cell2mat(cellfun(@(x) strcmp(x,calls{c}), txt, 'UniformOutput', 0));
            end
            call_index = cell2mat(call_index);
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            % Real work!
            % Iterate across each call type and look for occurrences of
            % pairs of each call type, direction matters.
            % Also make sure that the intercall interval is less than the
            % defined value "interval"
            % Then divide the counts by the total number of pairs for the
            % probabilities
            
            for c = 1:length(calls)
                idx_fir = call_index(:,c);
                idx_fir = [0;idx_fir(1:end-1)];
                idx_fir = logical(idx_fir);
                for cc = 1:length(calls)
                    idx_sec = call_index(idx_fir,cc);
                    idx_time = num(idx_fir) < interval;
                    tally(c,cc) = sum(idx_sec .* idx_time);
                end
                tally(c,:) = tally(c,:) / sum(tally(c,:));
            end
            
            % Make NaN values 0
            tally(isnan(tally)) = 0;
            
            % Position the results in a field named after the animal
            callprob{f}.(sheets{s}) = tally;     
            
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
        elseif size(num,1) == 1         % If there is only 1 call the animal is not usable
            callprob{f}.(sheets{s}) = 'single vocalization';
        elseif isempty(num)             % if there are no calls the animal is not usable
            callprob{f}.(sheets{s}) = 'no vocalizations';
        end     % for error checks
        
    end         % for the sheets
end             % for the files

end